const log = (msg) => {
   document.getElementById('log')
  .innerHTML = msg
const minLength = 10;
const maxLength = 10;
}

document.getElementById('00N4P00000FINaK')
  .addEventListener('keydown', (e) => {
    log('')
    
    const backspace = e.keyCode === 8
    const length = e.target.value.length
      + (backspace ? -1 : 1)
    
    if (length > maxLength && !backspace) {
      e.preventDefault()
    }

    if (length < minLength) {
      log(`minLength: ${minLength}`)
    } else {
      log('success')
    }
  
  })